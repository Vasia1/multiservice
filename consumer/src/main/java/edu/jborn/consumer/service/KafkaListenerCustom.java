package edu.jborn.consumer.service;

import edu.jborn.consumer.api.json.WordModel;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class KafkaListenerCustom {
    private final IncomingMessageService messageService;

    @KafkaListener(topics = "${application.kafka.topic}", groupId = "${application.kafka.group-id}")
    public void setMessageService(@NotNull WordModel message) {
        messageService.save(message.getWord());
    }
}
