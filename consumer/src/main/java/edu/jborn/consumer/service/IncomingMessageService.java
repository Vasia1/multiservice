package edu.jborn.consumer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.jborn.consumer.api.json.WordModel;
import edu.jborn.consumer.dao.MessageDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class IncomingMessageService {
    private final ObjectMapper om;
    private final MessageDAO messageDAO;

    public void save(String message) {
        try {
            var answer = messageDAO.add(om.readValue(message, WordModel.class).getWord());
            log.info("{}", answer);
        } catch (Exception e) {
            log.error("Error saving message", e);
        }
    }
}