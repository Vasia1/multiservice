package edu.jborn.consumer.api.controller;

import edu.jborn.consumer.service.IncomingMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api", produces = "application/json")
public class ConsumerController {
    private final IncomingMessageService service;

    @GetMapping("/send")
    public void getWord(@RequestParam String word) {
        service.save(word);
    }
}
