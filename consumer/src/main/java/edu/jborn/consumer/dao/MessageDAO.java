package edu.jborn.consumer.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

@Service
@Slf4j
public class MessageDAO {
    private final DataSource dataSource;

    public MessageDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Transactional
    public String add(String message) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(
                     "insert into kafka (message, create_date) values (?,?)")) {
            ps.setString(1, message);
            ps.setTimestamp(2, Timestamp.from(Instant.now()));
            int rs = ps.executeUpdate();
            if (rs == 1) {
                return String.format("message: %s was recorder", message);
            }
        } catch (SQLException e) {
            log.error("message: {} not record \n {}", message, e.getMessage());
        }
        return "message not record";
    }
}
