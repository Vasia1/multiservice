package edu.jborn.producer.runner;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProducerRunner implements ApplicationRunner {
    private final RequestGenerator requestGenerator;

    @Override
    public void run(ApplicationArguments args) {
        requestGenerator.generate();
    }
}
