package edu.jborn.producer.runner;

import edu.jborn.producer.model.WordModel;
import edu.jborn.producer.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
//@RequiredArgsConstructor
public class RequestGenerator {

    private final MessageService messageService;
    private final Random random = new Random();
    @Value("${period}")
    int period;

    public RequestGenerator(MessageService messageService) {
        this.messageService = messageService;
    }

    public void generate() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(() -> messageService.sendMessage(new WordModel(generateWords())),
                0, period, TimeUnit.SECONDS);
        log.info("START MESSAGE");
    }

    private String generateWords() {
        char[] word = new char[random.nextInt(100)];
        for (int i = 0; i < word.length; i++) {
            word[i] = (char) ('a' + random.nextInt(26));
        }
        return new String(word);
    }
}
