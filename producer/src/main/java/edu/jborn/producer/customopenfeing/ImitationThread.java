package edu.jborn.producer.customopenfeing;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "feignClient", url = "${consumerApi}")
public interface ImitationThread {

    @GetMapping()
    void send(@RequestParam String word);
}