package edu.jborn.producer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Configuration
public class JsonSerializerCustom<T> implements Serializer<T> {
    private final String encoding = StandardCharsets.UTF_8.name();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Serializer.super.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, @NonNull T data) {
        try {
            return new ObjectMapper().writeValueAsString(data).getBytes(encoding);
        } catch (Exception e) {
            throw new SerializationException("error when serializing String to byte[]", e);
        }
    }
}
