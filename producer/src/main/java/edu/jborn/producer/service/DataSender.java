package edu.jborn.producer.service;

import edu.jborn.producer.model.WordModel;

public interface DataSender {
    void send(WordModel wordModel);
}
