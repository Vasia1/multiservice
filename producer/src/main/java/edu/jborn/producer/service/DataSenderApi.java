package edu.jborn.producer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.jborn.producer.customopenfeing.ImitationThread;
import edu.jborn.producer.model.WordModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.errors.RetriableException;
import org.springframework.stereotype.Service;

@Service("api")
@Slf4j
@RequiredArgsConstructor
public class DataSenderApi implements DataSender {
    private final ImitationThread imitationThread;
    private final ObjectMapper om;

    @Override
    public void send(WordModel wordModel) {
        log.info("Send message to DataSenderApi");
        try {
            imitationThread.send(om.writeValueAsString(wordModel));
            log.info("{}", wordModel.getWord());
        } catch (RetriableException | JsonProcessingException re) {
            log.error("{}", re.getMessage());
        }
    }
}
