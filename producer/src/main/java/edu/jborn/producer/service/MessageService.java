package edu.jborn.producer.service;

import edu.jborn.producer.model.WordModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageService {
    private final Map<String, ? extends DataSender> threads;

    @Value("${target}")
    private String target;

    public void sendMessage(WordModel generatedWord) {
        log.info("Enter to Sender");
        try {
            var thread = threads.get(target);
            thread.send(generatedWord);
        } catch (RuntimeException e) {
            log.error("Target doesn't access", e);
        }

    }
}
