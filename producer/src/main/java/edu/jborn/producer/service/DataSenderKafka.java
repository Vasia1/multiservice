package edu.jborn.producer.service;

import edu.jborn.producer.model.WordModel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service("kafka")
@Slf4j
@RequiredArgsConstructor
public class DataSenderKafka implements DataSender {
    private final KafkaTemplate<String, Object> template;
    @Value("${application.kafka.topic}")
    private String topicName;

    @Override
    public void send(WordModel wordModel) {
        log.info("Send message to DataSenderKafka");
        try {
            template.send(topicName, wordModel)
                    .addCallback(new ListenableFutureCallback<>() {

                        @Override
                        public void onFailure(@NonNull Throwable ex) {
                            log.error("Message not send", ex);
                        }

                        @Override
                        public void onSuccess(SendResult<String, Object> result) {
                            log.info("{}", wordModel.getWord());
                        }
                    });
        } catch (Exception e) {
            log.error("send error, value:{}", wordModel, e);
        }
    }

}
